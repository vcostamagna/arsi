import os
import sys
import errno
import string
import subprocess
from androguard.core.bytecodes import art
try:
    from pyadb import ADB
except ImportError,e:
    # should never be reached
    print "[f] Required module missing. %s" % e.args[0]
    sys.exit(-1)

#change the following 2 paths
ADB_PATH = '/media/noflyzone/ANDROIDE/android-sdk-linux/platform-tools/adb'
API_MONITOR_PATH = '/media/noflyzone/ANDROIDE/APIMonitor-beta/apimonitor.py'

DEVICE_TMP = '/data/local/tmp'
WORKING_PATH = None
DALVIK_CACHE = '/data/dalvik-cache/arm'
#TO FIX
APK_PACKAGE_NAME = None
APK_OAT_PATH = None
APK_OAT_NAME = None
HOOKED_OAT_NAME = None

def get_working_path():
    """
    Creates and returns the path provided by the user
    """
    global WORKING_PATH
    tmp = os.getcwd()
    print "\t- Local destination [%s]: " % tmp ,
    destination = sys.stdin.readline().strip()
    if destination == '':
        destination = tmp
    
    if not destination[-1:] == '/':
        destination += '/' 
    
    try:
        os.mkdir(destination)
    except OSError,e:
        if e.errno == errno.EEXIST:
            pass
        else:
            print "\t- ERROR!: " , e.args
            return None
    except Exception,e:
        print "\t- ERROR!: " , e.mgs
        return None
    
    WORKING_PATH = destination.rstrip()
    return WORKING_PATH

def wait_device(adb):
	# get detected devices
	dev = 0
	while dev is 0:
	    print "[+] Detecting devices..." ,
	    error,devices = adb.get_devices()

	    if error is 1:
	        # no devices connected
	        print "No devices connected"
	        print "[+] Waiting for devices..."
	        adb.wait_for_device()
	        continue
	    elif error is 2:
	        print "You haven't enought permissions!"
	        exit(-3)
	        
	    print "OK"
	    dev = 1
	# this should never be reached
	if len(devices) == 0:
	    print "[+] No devices detected!"
	    exit(-4)
	# show detected devices
	i = 0
	for dev in devices:
	    print "\t%d: %s" % (i,dev)
	    i += 1
	# if there are more than one devices, ask to the user to choose one of them
	if i > 1:
	    dev = i + 1
	    while dev < 0 or dev > int(i - 1):
	        print "\n[+] Select target device [0-%d]: " % int(i - 1) ,
	        dev = int(stdin.readline())
	else:
	    dev = 0
	# set target device
	try:
	    adb.set_target_device(devices[dev])
	except Exception,e:
	    print "\n[!] Error:\t- ADB: %s\t - Python: %s" % (adb.get_error(),e.args)
	    exit(-5)
	#just for test
	print "\n[+] Using \"%s\" as target device" % devices[dev]
	print "Waiting for device..."
	adb.wait_for_device()
	err,dev = adb.get_devices()
	if len(dev) == 0:
	    print "Unexpected error, may be you're a very fast guy?"
	    sys.exit(-1)

	print "Selecting: %s" % dev[0]
	adb.set_target_device(dev[0])

def install_apk(adb,  target_apk):
	print "Installing apk..."
	cmd = "install -r"
	res = adb.install(reinstall=True, pkgapp=target_apk)
	#print res	

def prepare(adb, target_apk):
	global APK_PACKAGE_NAME
	global APK_OAT_NAME
	global APK_OAT_PATH
	global HOOKED_OAT_NAME
	#print "retriving oat file..."
	target_apk = os.path.basename(target_apk)
	tmp = target_apk.split('.apk')[0]
	#print tmp
	APK_PACKAGE_NAME = tmp.rstrip()
	HOOKED_OAT_NAME = '%s_hooked.oat' % (tmp)
	cmd = 'ls /data/dalvik-cache/arm/ | grep -i %s' % (tmp)
	name = adb.shell_command(cmd).rstrip()
	#print 'name vale: ' + name
	APK_OAT_NAME = name.rstrip()
	#print 'APK_OAT_NAME: ' + APK_OAT_NAME
	file = '/data/dalvik-cache/arm/%s' % (name.rstrip())
	APK_OAT_PATH = file
	print 'retrive: ' + file
	if WORKING_PATH is None:
		get_destination_path()
	#print 'dest: ' + WORKING_PATH

def pull_remote_oatfile(adb,file):
	res = adb.get_remote_file(file.rstrip(), WORKING_PATH)
	#print res	

def call_apimonitor(target_apk):
	print 'calling APIMonitor'
	cmd_list = [API_MONITOR_PATH,'-o',WORKING_PATH,target_apk]
	adb_proc = subprocess.Popen(cmd_list, stdin = subprocess.PIPE, \
	                      stdout = subprocess.PIPE, \
	                      stderr = subprocess.PIPE, shell = False)
	(__output, __error) = adb_proc.communicate()
	__return = adb_proc.returncode
	#print __return

def call_dex2oat(adb,remote_path):
	new_apk = '%s/%s_new.apk' % (WORKING_PATH, APK_PACKAGE_NAME)
	print new_apk
	res = adb.push_local_file(new_apk, remote_path)
	#print res
	dex2oat_cmd = ['dex2oat','--dex-file=%s' % (remote_path),'--oat-file=%s' % (DEVICE_TMP + '/' + HOOKED_OAT_NAME)]
	print dex2oat_cmd
	res = adb.shell_command(' '.join(dex2oat_cmd))
	#print res

def calcola_len_path_name(path):
	print path
	new_path = '%s/' % (DEVICE_TMP)
	l_tmp = len(DEVICE_TMP) + 1
	l_path = len(path)
	l_path = l_path - 4 # len('.apk')
	while l_tmp < l_path:
		new_path += str(1)
		l_tmp += 1
	new_path += '.apk'
	print new_path
	return new_path

if __name__ == "__main__":
	if len(sys.argv) > 1:
		target_apk = sys.argv[1]
	else:
		print 'usage: %s target_app.apk ' % (sys.argv[0])
		sys.exit(-1)

	adb = ADB()
	adb.set_adb_root()
	adb.set_system_rw() #optional
	# IMPORTANT: You should supply the absolute path to ADB binary 
	if adb.set_adb_path(ADB_PATH):
	    print "Version: %s" % adb.get_version()
	else:
	    print "Check ADB binary path"
	    sys.exit(-1)
	get_working_path()
	wait_device(adb)
	install_apk(adb,target_apk)
	prepare(adb,target_apk)
	pull_remote_oatfile(adb,APK_OAT_PATH)
	call_apimonitor(target_apk)

	oatf1 = art.ATformat(WORKING_PATH + APK_OAT_NAME)
	path1 = oatf1.get_oatdexfile_path()

	new_path = calcola_len_path_name(path1)
	#need to fix the path len
	call_dex2oat(adb, new_path)
	pull_remote_oatfile(adb, DEVICE_TMP + '/' + HOOKED_OAT_NAME)

	print 'ART apro: ' + WORKING_PATH  + HOOKED_OAT_NAME
	oatf2 = art.OATformat(WORKING_PATH  + HOOKED_OAT_NAME)
	
	ck1 = oatf1.get_oatdexfile_checksum()
	oatf2.set_oatdexfile_path(path1)
	oatf2.set_oatdexfile_checksum(ck1)
	oatf1.end()
	oatf2.end()

	print 'push oathooked: ' + WORKING_PATH + HOOKED_OAT_NAME + ' into: ' + APK_OAT_PATH
	res = adb.push_local_file(WORKING_PATH + HOOKED_OAT_NAME, APK_OAT_PATH)
